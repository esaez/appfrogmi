package cl.edison.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cl.edison.appfrogmi.R
import cl.edison.appfrogmi.model.direction.ModelDirection
import java.util.ArrayList

class ModelDirectionAdapter(context : Context, private val dataModelList : ArrayList<ModelDirection>) :
        RecyclerView.Adapter<ModelDirectionAdapter.MyViewHolder>(){

    private val inflater : LayoutInflater

    var onItemClick : ((ModelDirection) -> Unit)? = null

    init {
        inflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view = inflater.inflate(R.layout.stop_card, parent, false)
        return MyViewHolder(view)

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.tview_title_.setText(dataModelList[position].direction_name)
        holder.tview_id_.setText(dataModelList[position].agency_id)

    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var tview_title_: TextView
        var tview_id_: TextView


        init {

            tview_title_ = itemView.findViewById(R.id.txt_title) as TextView
            tview_id_ = itemView.findViewById(R.id.txt_id) as TextView

            itemView.setOnClickListener{

                onItemClick?.invoke(dataModelList[adapterPosition])

            }
        }

    }

    override fun getItemCount(): Int {
        return dataModelList.size
    }

}
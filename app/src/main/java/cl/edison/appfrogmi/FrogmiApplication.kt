package cl.edison.appfrogmi

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration

class FrogmiApplication : Application(){

    override fun onCreate() {
        super.onCreate()

        Realm.init(this)
        val configuration: RealmConfiguration = RealmConfiguration.Builder()
            .name(Realm.DEFAULT_REALM_NAME)
            .schemaVersion(0)
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.setDefaultConfiguration(configuration)

    }

}
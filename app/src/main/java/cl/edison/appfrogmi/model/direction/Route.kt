package cl.edison.appfrogmi.model.direction

import cl.edison.appfrogmi.model.stop.Direction

class Route {
    var routeLong_name: String? = null
    var route_type: String? = null
    var route_text_color: String? = null
    var agency_id: String? = null
    var route_id: String? = null
    var route_color: String? = null
    var route_desc: Any? = null
    var directions: List<Direction>? = null
    var route_url: String? = null
    var route_short_name: String? = null
}
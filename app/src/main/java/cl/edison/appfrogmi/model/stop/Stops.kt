package cl.edison.appfrogmi.model.stop

import cl.edison.appfrogmi.model.stop.Result

class Stops {

    var has_next: Boolean = false
    var page_number: Int? = null
    var total_results: Int? = null
    var total_pages: Int? = null
    var results: List<Result>? = null
    var page_size: Int? = null

}
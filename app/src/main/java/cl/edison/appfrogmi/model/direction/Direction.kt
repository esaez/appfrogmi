package cl.edison.appfrogmi.model.direction

class Direction {
    var direction_id: Int? = null
    var route_id: String? = null
    var direction_headsign: String? = null
    var direction_name: String? = null
}
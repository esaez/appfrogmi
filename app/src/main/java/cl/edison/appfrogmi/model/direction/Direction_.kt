package cl.edison.appfrogmi.model.direction

class Direction_ {
    var direction_id: Int? = null
    var route_d: String? = null
    var direction_headsign: String? = null
    var direction_name: String? = null
}
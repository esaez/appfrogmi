package cl.edison.appfrogmi.model.direction

class Result {
    var route: Route? = null
    var is_first_stop: Boolean? = null
    var is_last_stop: Boolean? = null
    var direction: Direction_? = null
}
package cl.edison.appfrogmi.model.stop

import cl.edison.appfrogmi.model.stop.Direction

class Result {

    var stop_lat: String? = null
    var stop_code: String? = null
    var stop_lon: String? = null
    var agency_id: String? = null
    var stop_id: String? = null
    var directions: List<Direction>? = null
    var stop_name: String? = null

}
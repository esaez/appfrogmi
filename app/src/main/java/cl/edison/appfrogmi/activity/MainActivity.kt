package cl.edison.appfrogmi.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cl.edison.adapter.ModelDirectionAdapter
import cl.edison.adapter.ModelStopAdapter
import cl.edison.appfrogmi.R
import cl.edison.appfrogmi.model.direction.Directions
import cl.edison.appfrogmi.model.direction.ModelDirection
import cl.edison.appfrogmi.model.stop.ModelStop
import cl.edison.appfrogmi.model.stop.Result
import cl.edison.appfrogmi.model.stop.Stops
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.StringRequestListener
import com.google.gson.Gson
import org.json.JSONObject


class MainActivity : AppCompatActivity() {

    val pathStops = "https://api.scltrans.it/v1/stops?"
    val pathDirections = "https://api.scltrans.it/v3/stops/%s/stop_routes"
    val lat: String = "-33.444087"
    val lon: String = "-70.653674"

    private var dataSetListStops: List<Result>? = ArrayList()
    private var dataSetListDirections: List<cl.edison.appfrogmi.model.direction.Result>? =
        ArrayList()
    private var dataModelListSto: ArrayList<ModelStop>? = null
    private var dataModelListDirec: ArrayList<ModelDirection>? = null
    private var adapterStop: ModelStopAdapter? = null
    private var adapterDirection: ModelDirectionAdapter? = null
    private var stopListSet: ArrayList<ModelStop>? = null

    private var recyclerViewStop: RecyclerView? = null
    private var recyclerViewDirections: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerViewStop = findViewById(R.id.rv_stops) as RecyclerView
        recyclerViewDirections = findViewById(R.id.rv_directions) as RecyclerView

        retrieveStops()
    }

    override fun onBackPressed() {
        if (recyclerViewDirections!!.visibility == View.VISIBLE) {
            recyclerViewDirections!!.visibility = View.GONE
        }else{
            super.onBackPressed()
        }
    }

    private fun retrieveStops() {

        var stops: Stops

        AndroidNetworking.get(pathStops)
            .addQueryParameter("center_lat", lat)
            .addQueryParameter("center_lon", lon)
            .addQueryParameter("limit", "20")
            .build()
            .getAsString(object : StringRequestListener {
                override fun onResponse(response: String) { // do anything with response
                    stops = Gson().fromJson(response, Stops::class.java)

                    dataSetListStops = stops.results

                    dataModelListSto = populateStops()
                    adapterStop = ModelStopAdapter(applicationContext, dataModelListSto!!)
                    recyclerViewStop!!.adapter = adapterStop
                    recyclerViewStop!!.layoutManager =
                        LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)

                    adapterStop?.onItemClick = { model ->

                        retrieveDirections(model.stop_id)

                    }

                }

                override fun onError(error: ANError) { // handle error
                    Log.e("resp", error.errorBody)
                }
            })

    }


    private fun populateStops(): ArrayList<ModelStop> {

        val list = ArrayList<ModelStop>()
        var i = 0

        dataSetListStops?.forEach {

            val modelStop = ModelStop()
            modelStop.stop_name = it.stop_name
//            modelStop.direction_headsign = it.directions?.get(i)?.direction_headsign
            modelStop.stop_id = it.stop_id

            list.add(modelStop)
            i++
        }

        return list
    }

    private fun retrieveDirections(stopId: String?) {

        var directions: Directions
        var pathUrl = String.format(pathDirections, stopId)

        AndroidNetworking.get(pathUrl)
            .build()
            .getAsString(object : StringRequestListener {
                override fun onResponse(response: String) { // do anything with response
                    Log.e("resp", response)
                    directions = Gson().fromJson(response, Directions::class.java)

                    dataSetListDirections = directions.results

                    dataModelListDirec = populateDirectios()
                    adapterDirection =
                        ModelDirectionAdapter(applicationContext, dataModelListDirec!!)
                    recyclerViewDirections!!.adapter = adapterDirection
                    recyclerViewDirections!!.layoutManager =
                        LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
                    recyclerViewDirections!!.visibility = View.VISIBLE

                }

                override fun onError(error: ANError) { // handle error
                    Log.e("resp", error.errorBody)
                }
            })

    }


    private fun populateDirectios(): ArrayList<ModelDirection> {

        val list = ArrayList<ModelDirection>()
        var i = 0

        dataSetListDirections?.forEach {

            val modelDirections = ModelDirection()
            modelDirections.direction_name = it.direction?.direction_headsign
            modelDirections.agency_id = it.route?.agency_id
            list.add(modelDirections)
            i++
        }

        return list
    }
}
